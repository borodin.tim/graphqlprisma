const users = [{
    id: '1',
    name: 'Tim',
    email: 'tim@mail.com',
    age: 32
}, {
    id: '2',
    name: 'Mara',
    email: 'mara@mail.com'
}, {
    id: '3',
    name: 'Harley',
    email: 'harley@mail.com'
}];

const posts = [{
    id: '11',
    title: 'Post one',
    body: 'Post one body',
    published: true,
    author: '1'
}, {
    id: '22',
    title: 'Post two',
    body: 'Post two body',
    published: false,
    author: '1'
}, {
    id: '33',
    title: 'Post three',
    body: 'Post three body',
    published: true,
    author: '2'
}];

const comments = [{
    id: '111',
    text: 'This is comment 1',
    author: '1',
    post: '33'
}, {
    id: '222',
    text: 'This is comment 2',
    author: '2',
    post: '22'
}, {
    id: '333',
    text: 'Comment 3',
    author: '3',
    post: '11'
}, {
    id: '444',
    text: 'Comment 4',
    author: '3',
    post: '11'
}, {
    id: '555',
    text: 'Comment 5',
    author: '3',
    post: '33'
}];

const db = { users, posts, comments };

export default db;