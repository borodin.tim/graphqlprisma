import 'cross-fetch/polyfill';
import seedDatabase, {
    userOne,
    userTwo,
    commentOne,
    commentTwo,
    postOne,
    postTwo
} from './utils/seedDatabase';
import getClient from './utils/getClient';
import prisma from '../src/prisma';
import {
    deleteComment,
    subscribeToComments,
    getPost,
    createComment,
    updateComment
} from './utils/operations'

const client = getClient();

beforeEach(seedDatabase);

test('Should delete own comment', async () => {
    const client = getClient(userTwo.jwt);
    const variables = {
        id: commentOne.comment.id
    };

    const deletedComment = await client.mutate({
        mutation: deleteComment,
        variables
    });
    const commentExists = await prisma.exists.Comment({ id: commentOne.comment.id })

    expect(deletedComment.data.deleteComment.id).toBe(commentOne.comment.id);
    expect(commentExists).toBe(false);
})

test('Should not delete other users comments', async () => {
    const client = getClient(userTwo.jwt);
    const variables = {
        id: commentTwo.comment.id
    };

    await expect(client.mutate({
        mutation: deleteComment,
        variables
    })).rejects.toThrow();
    const commentExists = await prisma.exists.Comment({ id: commentTwo.comment.id });

    expect(commentExists).toBe(true);
})

test('Should subscribe to comments for a post', async (done) => {
    const variables = {
        postId: postOne.post.id
    };
    client.subscribe({
        query: subscribeToComments,
        variables
    }).subscribe({
        next(response) {
            expect(response.data.comment.mutation).toBe('DELETED');
            done();
        }
    });

    await prisma.mutation.deleteComment({ where: { id: commentOne.comment.id } });
})

test('Should fetch post comments', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        id: postOne.post.id
    }
    const post = await client.query({
        query: getPost,
        variables
    });
    expect(post.data.post.comments.length).toBe(2);
})

test('Should create a new comment', async () => {
    const client = getClient(userTwo.jwt);
    const text = 'This is a new comment text';
    const variables = {
        data: {
            text,
            post: postOne.post.id
        }
    }
    const comment = await client.mutate({
        mutation: createComment,
        variables
    });
    const comments = await prisma.query.comments({
        where: {
            post: {
                id: postOne.post.id
            }
        }
    })

    expect(comment.data.createComment.text).toBe(text);
    expect(comments.length).toBe(3);
})

test('Should not create comment on draft post', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        data: {
            text: 'A comment we will never see',
            post: postTwo.post.id
        }
    };
    await expect(client.mutate({
        mutation: createComment,
        variables
    })).rejects.toThrow();
})

test('Should update comment', async () => {
    const client = getClient(userOne.jwt);
    const newBody = 'Body of comment 1 is now modified!';
    const variables = {
        id: commentTwo.comment.id,
        data: {
            text: newBody
        }
    };

    const comment = await client.mutate({
        mutation: updateComment,
        variables
    });
    expect(comment.data.updateComment.text).toBe(newBody);
})

test('Should not update another users comment', async () => {
    const client = getClient(userOne.jwt);
    const newBody = 'Body of comment 1 is now modified!';
    const variables = {
        id: commentOne.comment.id,
        data: {
            text: newBody
        }
    };

    await expect(client.mutate({
        mutation: updateComment,
        variables
    })).rejects.toThrow();
})


test('Should not delete another users comment', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        id: commentOne.comment.id
    };
    await expect(client.mutate({
        mutation: deleteComment,
        variables
    })).rejects.toThrow();
})

test('Should require authentication to create a comment', async () => {
    const variables = {
        data: {
            text: 'Some text',
            post: postOne.post.id
        }
    };
    await expect(client.mutate({
        mutation: createComment,
        variables
    })).rejects.toThrow();
})

test('Should require authentication to update a comment', async () => {
    const variables = {
        id: commentTwo.comment.id,
        data: {
            text: 'Some updated text',
        }
    };
    await expect(client.mutate({
        mutation: updateComment,
        variables
    })).rejects.toThrow();
})

test('Should require authentication to delete a comment', async () => {
    const variables = {
        id: commentTwo.comment.id
    };
    await expect(client.mutate({
        mutation: deleteComment,
        variables
    })).rejects.toThrow();
})