import 'cross-fetch/polyfill';
import prisma from '../src/prisma';
import seedDatabase, { userOne, userTwo, postOne, postTwo } from './utils/seedDatabase';
import getClient from './utils/getClient';
import {
    getPosts,
    getMyPosts,
    updatePost,
    createPost,
    deletePost,
    subscribeToPosts,
    getPost
} from './utils/operations';
import { extractFragmentReplacements } from 'prisma-binding';

const client = getClient();

beforeEach(seedDatabase);

test('Should get published posts only', async () => {
    jest.setTimeout(20000);
    const response = await client.query({ query: getPosts });

    expect(response.data.posts.length).toBe(1);
    expect(response.data.posts[0].title).toBe('Post one');
    expect(response.data.posts[0].published).toBe(true);
})

test('Should return my posts', async () => {
    const client = getClient(userOne.jwt);
    const { data } = await client.query({ query: getMyPosts });

    expect(data.myPosts.length).toBe(2);
})

test('Should be able to update own post', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        id: postOne.post.id,
        data: {
            published: false
        }
    };

    const updatedPost = await client.mutate({
        mutation: updatePost,
        variables
    });
    const postExists = await prisma.exists.Post({
        id: postOne.post.id,
        published: false
    });

    expect(updatedPost.data.updatePost.published).toBe(false);
    expect(postExists).toBe(true);
})

test('Should create post', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        data: {
            title: "Test post",
            body: "Test post body",
            published: true
        }
    };

    const createdPost = await client.mutate({ mutation: createPost, variables: variables });
    const postExists = await prisma.exists.Post({
        title: "Test post",
        body: "Test post body",
        published: true
    });

    expect(createdPost.data.createPost.title).toEqual("Test post");
    expect(createdPost.data.createPost.body).toEqual("Test post body");
    expect(createdPost.data.createPost.published).toEqual(true);
    expect(postExists).toBe(true);
})

test('Should delete post', async () => {
    const client = getClient(userOne.jwt)
    const variables = {
        id: postTwo.post.id
    };

    const deletedPost = await client.mutate({ mutation: deletePost, variables });
    const postExists = await prisma.exists.Post({ id: postTwo.post.id });

    expect(deletedPost.data.deletePost.id).toBe(postTwo.post.id);
    expect(postExists).toBe(false);
})

// test('Should subscribe to changes for published posts - UPDATED', async (done) => {
//     client.subscribe({
//         query: subscribeToPosts
//     }).subscribe({
//         next(response) {
//             expect(response.data.post.mutation).toBe('UPDATED');
//             done();
//         }
//     });

//     await prisma.mutation.updatePost({
//         where: {
//             id: postOne.post.id,
//         },
//         data: {
//             title: "Post 1 title changed for subscription text"
//         }
//     });
// })

// test('Should subscribe to changes for published posts - DELETED', async (done) => {
//     client.subscribe({
//         query: subscribeToPosts
//     }).subscribe({
//         next(response) {
//             expect(response.data.post.mutation).toBe('DELETED');
//             done();
//         }
//     });

//     await prisma.mutation.deletePost({
//         where: {
//             id: postTwo.post.id,
//         }
//     });
// })

test('Should not be able to update another users post', async () => {
    const client = getClient(userTwo.jwt);
    const variables = {
        id: postOne.post.id,
        data: {
            title: 'Changed  title'
        }
    }
    await expect(client.mutate({
        mutation: updatePost,
        variables
    })).rejects.toThrow();
    const modifiedPostExists = await prisma.exists.Post({
        id: postOne.post.id,
        title: 'Changed  title'
    });
    expect(modifiedPostExists).toBe(false);
})

test('Should not be able to delete another users post', async () => {
    const client = getClient(userTwo.jwt);
    const variables = {
        id: postTwo.post.id
    }
    await expect(client.mutate({
        mutation: deletePost,
        variables
    })).rejects.toThrow();

    const modifiedPostExists = await prisma.exists.Post({
        id: postTwo.post.id,
    });
    expect(modifiedPostExists).toBe(true);
})

test('Should require authentication to create a post', async () => {
    const variables = {
        data: {
            title: "Title1",
            body: "Body1",
            published: true
        }
    };
    await expect(client.mutate({
        mutation: createPost,
        variables
    })).rejects.toThrow();
})

test('Should require authentication to update a post', async () => {
    const variables = {
        id: postOne.post.id,
        data: {
            title: "Title1",
            body: "Body1",
            published: true
        }
    };
    await expect(client.mutate({
        mutation: updatePost,
        variables
    })).rejects.toThrow();
})

test('Should require authentication to delete a post', async () => {
    const variables = {
        id: postTwo.post.id,
    };
    await expect(client.mutate({
        mutation: deletePost,
        variables
    })).rejects.toThrow();
})

test('Should fetch published post by id', async () => {
    const variables = {
        id: postOne.post.id
    };
    const post = await client.query({
        query: getPost,
        variables
    })
    expect(post.data.post.id).toBe(postOne.post.id);
})

test('Should fetch own post by id', async () => {
    const client = getClient(userOne.jwt);
    const variables = {
        id: postOne.post.id
    };
    const post = await client.query({
        query: getPost,
        variables
    });
    expect(post.data.post.id).toBe(postOne.post.id);
})

test('Should not fetch draft post from other user', async () => {
    const client = getClient(userTwo.jwt);
    const variables = {
        id: postTwo.post.id
    };
    await expect(client.query({
        query: getPost,
        variables
    })).rejects.toThrow();
})