import { gql } from 'apollo-boost';

/* User */
const createUser = gql`
    mutation($data: CreateUserInput!){
        createUser(data: $data){
            token, 
            user{
                id
                name
                email
            }
        }
    }
`;

const getUsers = gql`
    query{
        users{
            id
            name
            email
        }
    }
`;

const login = gql`
mutation($data: LoginUserInput!){
    login(data: $data){
       token
    }
}
`;

const getProfile = gql`
query{
    me{
        id
        name
        email
    }
}
`;

/* Post */
const getPosts = gql`
    query{
        posts{
            id
            title
            body
            published
        }
    }
`;

const getPost = gql`
    query($id: ID!){
        post(id: $id){
            id
            title
            body
            published
            comments{
                id
                text
                author{
                    id
                    name
                }
            }
        }
    }
`;

const getMyPosts = gql`
    query{
        myPosts{
            id
            title
            body
            published
        }
    }
`;

const updatePost = gql`
    mutation($id: ID!, $data: UpdatePostInput!){
        updatePost(
            id: $id,
            data: $data
        ){
            id
            title
            body
            published
        }
    }
`;

const createPost = gql`
    mutation($data: CreatePostInput!){
        createPost( 
            data: $data
            ){
            id
            title
            body
            published
        }
    }
`;
const deletePost = gql`
mutation($id: ID!){
    deletePost(
        id: $id
    ){
        id
        title
        body
        published
    }
}
`;

const subscribeToPosts = gql`
    subscription{
        post{
            mutation
        }
    }
`;

/* Comment */
const createComment = gql`
    mutation($data: CreateCommentInput!){
        createComment(data: $data){
            id
            text
        }
    }
`;

const updateComment = gql`
    mutation($id: ID!, $data: UpdateCommentInput!){
        updateComment(id: $id, data: $data){
            text
        }
    }
`;

const deleteComment = gql`
    mutation($id: ID!){
        deleteComment(id: $id){
            id
            text
        }
    }
`;

const subscribeToComments = gql`
    subscription($postId: ID!) {
        comment(postId: $postId){
            mutation
            node{
                id
                text
            }
        }
    }
`;

export {
    createUser,
    login,
    getUsers,
    getProfile,
    getPosts,
    getPost,
    getMyPosts,
    updatePost,
    createPost,
    deletePost,
    createComment,
    updateComment,
    deleteComment,
    subscribeToComments,
    subscribeToPosts
}