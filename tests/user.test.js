import 'cross-fetch/polyfill';
import prisma from '../src/prisma';
import seedDatabase, { userOne } from './utils/seedDatabase';
import getClient from './utils/getClient';
import { createUser, login, getUsers, getProfile, getMyPosts } from './utils/operations';

const client = getClient();

beforeEach(seedDatabase);

test('Should create a new user', async () => {
    const variables = {
        data: {
            name: "Tim",
            email: "tim@mail.com",
            password: "12345678"
        }
    }
    const response = await client.mutate({
        mutation: createUser,
        variables: variables
    });
    const userExists = await prisma.exists.User({ id: response.data.createUser.user.id });

    expect(userExists).toBe(true);
})

test('Should expose public author profiles', async () => {
    const response = await client.query({ query: getUsers });
    expect(response.data.users.length).toBe(2);
    expect(response.data.users[0].name).toBe('Bob');
    expect(response.data.users[0].email).toBe(null);
    expect(response.data.users[1].name).toBe('John');
    expect(response.data.users[1].email).toBe(null);
})

test('Should not login with bad credentials', async () => {
    const variables = {
        data: {
            email: "definitelyNotBob@mail.com",
            password: "theIncorrectPW"
            // email: userOne.user.email,
            // password: 'asd12345'
        }
    };

    await expect(client.mutate({
        mutation: login,
        variables
    })).rejects.toThrow();
})

test('Should not register user with short password', async () => {
    const variables = {
        data: {
            name: 'UniqueUserName69261',
            email: 'email@mail.com',
            password: 'pwd'
        }
    };

    await expect(client.mutate({ mutation: createUser, variables })).rejects.toThrow();
})

test('Should fetch user profile', async () => {
    const client = getClient(userOne.jwt);
    const { data } = await client.query({ query: getProfile });

    expect(data.me.id).toBe(userOne.user.id);
    expect(data.me.name).toBe(userOne.user.name);
    expect(data.me.email).toBe(userOne.user.email);
})

test('Should not signup a user with email that is already in use', async () => {
    const variables = {
        data: {
            name: 'UserWithTakenEmail8163',
            email: userOne.user.email,
            password: '12345789'
        }
    }
    await expect(client.mutate({ mutation: createUser, variables })).rejects.toThrow();
    const userExists = await prisma.exists.User({ name: variables.data.name });
    expect(userExists).toBe(false);
})

test('Should login and provide authentication token', async () => {
    const variables = {
        data: {
            email: userOne.user.email,
            password: 'asd12345'
        }
    }

    const response = await client.mutate({ mutation: login, variables });
    expect(response.data.login.token).not.toBe(null);
})

test('Should reject me query without authentication', async () => {
    await expect(client.query({
        query: getMyPosts
    })).rejects.toThrow();
})

test('Should hide emails when fetching list of users', async () => {
    const users = await client.query({ query: getUsers });
    expect(users.data.users[0].email).toBe(null);
    expect(users.data.users[1].email).toBe(null);
})